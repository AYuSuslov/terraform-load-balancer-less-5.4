terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
  }
}

provider "google" {
  project     = var.gcp_project
  region      = var.gcp_region
  zone        = var.gcp_zone
  credentials = var.gcp_creds
}

provider "cloudflare" {
  email       = var.cloudflare_email
  api_key     = var.cloudflare_api_key
}
