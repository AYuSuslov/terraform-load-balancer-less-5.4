resource "google_compute_instance_group" "tf_lb_instances" {
  name      = "lb-instances-group"
  zone      = var.gcp_zone
  instances = "${google_compute_instance.vm_instance_lb.*.id}"
  lifecycle {
    create_before_destroy = true
  }
}

resource "google_compute_backend_service" "tf_lb_backends" {
  name          = "lb-backend-service"
  health_checks = [google_compute_http_health_check.tf_lb_hc.id]
  backend {
    group       = google_compute_instance_group.tf_lb_instances.id
  }
}

resource "google_compute_http_health_check" "tf_lb_hc" {
  name = "lb-health-check"
}

resource "google_compute_url_map" "tf_lb_urlmap" {
  name = "lb-url-map"
  default_service = google_compute_backend_service.tf_lb_backends.id
}

resource "google_compute_target_http_proxy" "tf_lb_proxy" {
  name    = "lb-http-proxy"
  url_map = google_compute_url_map.tf_lb_urlmap.id
}

resource "google_compute_global_forwarding_rule" "tf_lb_rule" {
  name       = "lb-forwarding-rule"
  target     = google_compute_target_http_proxy.tf_lb_proxy.id
  ip_address = google_compute_global_address.tf_lb_addr.id
  port_range = "80"
}

resource "google_compute_global_address" "tf_lb_addr" {
  name = "global-lb-address"
}

resource "cloudflare_record" "juneway" {
  zone_id = var.cloudflare_zone_id
  name = "ayususlov.juneway.pro"
  value = google_compute_global_address.tf_lb_addr.address
  type = "A"
  ttl = 3600
}
