resource "null_resource" "command_delete_hostfile" {
  provisioner "local-exec" {
    command = "rm -f host.list"
  }
}

resource "google_compute_disk" "test_disk_8" {
  count = var.node_count
  name  = "disk-8-${count.index}"
  type  = "pd-standard"
  size  = 8
  zone  = var.gcp_zone
}

resource "google_compute_disk" "test_disk_10" {
  count = var.node_count
  name  = "disk-10-${count.index}"
  type  = "pd-standard"
  size  = 10
  zone  = var.gcp_zone
}

data "google_compute_image" "debian_image" {
  family  = "debian-10"
  project = "debian-cloud"
}

resource "google_compute_attached_disk" "test_8" {
  count    = var.node_count
  disk     = google_compute_disk.test_disk_8["${count.index}"].id
  instance = google_compute_instance.vm_instance_lb["${count.index}"].id
}

resource "google_compute_attached_disk" "test_10" {
  count    = var.node_count
  disk     = google_compute_disk.test_disk_10["${count.index}"].id
  instance = google_compute_instance.vm_instance_lb["${count.index}"].id
}

resource "google_compute_instance" "vm_instance_lb" {
  count        = var.node_count
  name         = "vm-instance-${count.index}"
  machine_type = "e2-medium"
  tags         = ["terraform-servers"]

  network_interface {
    network = "default"
    access_config {
    }
  }

  boot_disk {
    initialize_params {
      image = data.google_compute_image.debian_image.self_link
    }
  }

  provisioner "local-exec" {
    command = "echo ${self.name} ${self.network_interface.0.access_config.0.nat_ip} >> host.list"
  }

  connection {
    user = "suslov_ubuntu"
    timeout = "2m"
    host = self.network_interface.0.access_config.0.nat_ip
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get -y update",
      "sudo apt-get -y install docker.io",
      "sudo docker run -v /usr/share/nginx/html/:/usr/share/nginx/html/ -p 80:80 -d nginx:latest",
      "echo Juneway ${self.network_interface.0.access_config.0.nat_ip} ${self.name} | sudo tee /usr/share/nginx/html/index.html",
    ]
  }
}
